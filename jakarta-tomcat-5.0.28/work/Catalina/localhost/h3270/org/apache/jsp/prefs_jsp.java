package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;

public final class prefs_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.Vector _jspx_dependants;

  public java.util.List getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\n');
      org.h3270.web.SessionState sessionState = null;
      synchronized (session) {
        sessionState = (org.h3270.web.SessionState) _jspx_page_context.getAttribute("sessionState", PageContext.SESSION_SCOPE);
        if (sessionState == null){
          sessionState = new org.h3270.web.SessionState();
          _jspx_page_context.setAttribute("sessionState", sessionState, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"  \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>Preferences</title>\n");
      out.write("\n");
      out.write("<style type=\"text/css\">\n");
      out.write("td { font-family:freesans,arial,helvetica; }\n");
      out.write("</style>\n");
      out.write("\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("  function doApply() {\r\n");
      out.write("    var control = opener.document.forms[\"control\"];\r\n");
      out.write("    var prefs = document.forms[\"prefs\"];\n");
      out.write("    control.colorscheme.value\n");
      out.write("      = prefs.colorscheme.options[prefs.colorscheme.selectedIndex].text;\n");
      out.write("\n");
      out.write("\tcontrol.font.value\n");
      out.write("\t  = prefs.font.options[prefs.font.selectedIndex].value;\n");
      out.write("\n");
      out.write("    if (prefs.render.checked) {\n");
      out.write("      control.render.value = \"true\";\n");
      out.write("    } else {\n");
      out.write("      control.render.value = \"false\";\n");
      out.write("    }\n");
      out.write("    control.submit();\n");
      out.write("  }\n");
      out.write("\n");
      out.write("  function doSubmit() {\n");
      out.write("    doApply();\n");
      out.write("    window.close();\n");
      out.write("  }\n");
      out.write("</script>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("  <form id=\"prefs\" action=\"\" method=\"post\">\n");
      out.write("  <table cellspacing=\"4\" style=\"font-size:10pt;\">\n");
      out.write("    <tr>\n");
      out.write("      <td>Color Scheme:</td>\n");
      out.write("      <td>\n");
      out.write("        <select name=\"colorscheme\" style=\"min-width:12em;\">\n");
      out.write("          ");

 			Iterator i = sessionState.getColorschemeSelectOptions(request);
 			while(i.hasNext()) { 
      out.write("\r\n");
      out.write(" \t\t\t  ");
      out.print( i.next() );
      out.write("\n");
      out.write(" \t      ");
 } 
      out.write("\t\n");
      out.write("        </select>\n");
      out.write("      </td>\n");
      out.write("    </tr>\n");
      out.write("    <tr>\n");
      out.write("      <td>Font:</td>\n");
      out.write("      <td>\n");
      out.write("        <select name=\"font\" style=\"min-width:12em;\">\n");
      out.write("        ");
 i = sessionState.getFontSelectOptions();
           while(i.hasNext()) { 
      out.write("\n");
      out.write("\t\t\t\t");
      out.print( i.next() );
      out.write("          \n");
      out.write("\t\t");
 } 
      out.write("\n");
      out.write("        </select>\r\n");
      out.write("      </td>\n");
      out.write("    </tr>\n");
      out.write("<!--    <tr>\n");
      out.write("      <td>Charset:</td>\n");
      out.write("      <td>\n");
      out.write("      <select name=charset style=\"min-width:12em;\" selected=german>\n");
      out.write("        <option>apl</option>\n");
      out.write("        <option>belgian</option>\n");
      out.write("        <option>bracket</option>\n");
      out.write("        <option>brazilian</option>\n");
      out.write("        <option>finnish</option>\n");
      out.write("        <option>french</option>\n");
      out.write("        <option selected>german</option>\n");
      out.write("        <option>icelandic</option>\n");
      out.write("        <option>hebrew</option>\n");
      out.write("        <option>iso-turkish</option>\n");
      out.write("        <option>italian</option>\n");
      out.write("        <option>japanese</option>\n");
      out.write("        <option>norwegian</option>\n");
      out.write("        <option>russian</option>\n");
      out.write("        <option>simplified-chinese</option>\n");
      out.write("        <option>slovenian</option>\n");
      out.write("        <option>thai</option>\n");
      out.write("        <option>uk</option>\n");
      out.write("        <option>us-intl</option>\n");
      out.write("      </select>\n");
      out.write("      </td>\n");
      out.write("    </tr> -->\n");
      out.write("    <tr>\n");
      out.write("      <td colspan=\"2\">\n");
      out.write("        <input type=\"checkbox\" name=\"render\" value=\"render\"\n");
      out.write("        ");
 if (sessionState.useRenderers())
             out.print (" checked=\"checked\" ");
        
      out.write("\n");
      out.write("        /> Use Regex Rendering Engine\n");
      out.write("      </td>\n");
      out.write("    </tr>\n");
      out.write("    <tr>\n");
      out.write("      <td colspan=\"3\" align=\"right\"  style=\"padding-top:1em;\">\n");
      out.write("        <input type=\"button\" id=\"prefs-ok\" name=\"prefs-ok\" value=\"OK\"\n");
      out.write("               onclick=\"doSubmit();\" />\n");
      out.write("        <input type=\"button\" id=\"prefs-apply\" name=\"prefs-apply\" value=\"Apply\"\n");
      out.write("               onclick=\"doApply();\" />\n");
      out.write("        <input type=\"submit\" name=\"prefs-cancel\" value=\"Cancel\" \n");
      out.write("               onclick=\"window.close();\" />\n");
      out.write("      </td>\n");
      out.write("    </tr>\n");
      out.write("  </table>\n");
      out.write("  </form>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
