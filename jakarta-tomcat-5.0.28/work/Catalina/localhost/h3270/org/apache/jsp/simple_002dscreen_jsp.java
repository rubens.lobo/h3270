package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class simple_002dscreen_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.Vector _jspx_dependants;

  static {
    _jspx_dependants = new java.util.Vector(3);
    _jspx_dependants.add("/common/h3270-css.jsp");
    _jspx_dependants.add("/common/h3270-screen.jsp");
    _jspx_dependants.add("/common/h3270-control.jsp");
  }

  public java.util.List getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      org.h3270.web.SessionState sessionState = null;
      synchronized (session) {
        sessionState = (org.h3270.web.SessionState) _jspx_page_context.getAttribute("sessionState", PageContext.SESSION_SCOPE);
        if (sessionState == null){
          sessionState = new org.h3270.web.SessionState();
          _jspx_page_context.setAttribute("sessionState", sessionState, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"  \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
      out.write("<html>\n");
      out.write("  <head>\n");
      out.write("    <title>h3270</title>\n");
      out.write("    <meta http-equiv=\"expires\" content=\"0\">\n");
      out.write("    <style type=\"text/css\">\n");
      out.write("      ");
      out.write("\n");
      out.write("pre, pre input, textarea {\n");
      out.write("\tfont-size: 10pt;\n");
      out.write("\tborder-width: 0pt;\n");
      out.write("}\n");
      out.write("\n");
      out.write("pre, pre input, textarea {\n");
      out.write("    font-family: ");
      out.print( sessionState.getFontName() );
      out.write("; \n");
      out.write("}\n");
      out.write("\n");
      out.write(".h3270-screen-border {\n");
      out.write("    border-style:solid; \n");
      out.write("    border-width:1px;\n");
      out.write("    border-collapse:collapse;\n");
      out.write("}\n");
      out.write("\n");
      out.print( sessionState.getActiveColorScheme(request).toCSS() );
      out.write('\n');
      out.write("\n");
      out.write("    </style>\n");
      out.write("\n");
      out.write("    <script src=\"common/keyboard.js\" type=\"text/javascript\"></script>\n");
      out.write("    <script type=\"text/javascript\" >\n");
      out.write("      function openPrefs() {\n");
      out.write("        prefsWindow = window.open (\"");
      out.print( response.encodeURL("prefs.jsp") );
      out.write("\",\n");
      out.write("                                   \"Preferences\",\n");
      out.write("                                   \"width=280,height=170,left=500,top=300\");\n");
      out.write("        if (prefsWindow.opener == null)\n");
      out.write("          prefsWindow.opener = self;\n");
      out.write("      }\n");
      out.write("    </script>\n");
      out.write("  </head>\n");
      out.write("\n");
      out.write("  <body>\n");
      out.write("    ");
      out.write("<table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\">\n");
      out.write("  <tr>\n");
      out.write("    <td style=\"width:50em;height:37em;\" align=\"center\" valign=\"middle\"\n");
      out.write("      class=\"h3270-screen-border h3270-form\">\n");
      out.write("      ");
      out.print( sessionState.getScreen(request) );
      out.write("\n");
      out.write("    </td>\n");
      out.write("\n");
      out.write("    ");
 if (sessionState.useKeypad(request)) { 
      out.write("\n");
      out.write("       <td rowspan=2 valign=top\n");
      out.write("           class=\"h3270-screen-border\">\n");
      out.write("         ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "keys.jsp", out, true);
      out.write("\n");
      out.write("       </td>\n");
      out.write("    ");
 } 
      out.write("\n");
      out.write("\n");
      out.write("  </tr>\n");
      out.write("  <tr>\n");
      out.write("    <td align=\"left\" valign=\"bottom\" class=\"h3270-screen-border\">\n");
      out.write("      ");
      out.write("        <table style=\"width:100%;\">\n");
      out.write("          <tr>\n");
      out.write("            <form name=\"control\" \n");
      out.write("                  action=\"");
      out.print( response.encodeURL("servlet") );
      out.write("\"\n");
      out.write("                  method=POST>\n");
      out.write("            <td width=30% align=left>\n");
      out.write("              ");
 if (!sessionState.isConnected(request)) {
                   String targetHost = 
                     (String)session.getAttribute("targetHost");
                   if (targetHost == null) { 
      out.write("\n");
      out.write("                     Host:&nbsp;<input type=text\n");
      out.write("                                       style=\"background-color:lightgrey;\"\n");
      out.write("                     ");
 String hostname = request.getParameter("hostname");
                        if (hostname != null) {
                          out.println("value=\"" + hostname + "\"");
                        } 
      out.write("\n");
      out.write("                                       name=hostname>\n");
      out.write("                 ");
 } else { 
      out.write("\n");
      out.write("                     Host: <b>");
      out.print( targetHost );
      out.write("</b>\n");
      out.write("                     <input type=hidden value=");
      out.print( targetHost );
      out.write(" name=hostname>\n");
      out.write("                 ");
 } 
      out.write("\n");
      out.write("            </td>\n");
      out.write("            <td width=70% align=right>\n");
      out.write("                 <input type=submit name=connect value=\"Connect\">\n");
      out.write("                 <input type=button id=\"prefs\" name=prefs value=\"Preferences...\"\n");
      out.write("                        onClick=\"openPrefs();\">\n");
      out.write("            ");
 } else { 
      out.write("\n");
      out.write("                 Host: <b>");
      out.print( sessionState.getHostname(request) );
      out.write("</b>\n");
      out.write("              </td>\n");
      out.write("              <td width=70% align=right>\n");
      out.write("                 <input type=submit name=disconnect value=\"Disconnect\">\n");
      out.write("                 <input type=submit name=refresh value=\"Refresh\">\n");
      out.write("                 <input type=hidden name=dumpfile value=\"\">\n");
      out.write("                 <input type=button name=dump value=\"Dump\"\n");
      out.write("                        onClick=\"document.control.dumpfile.value=prompt('Filename:',''); document.control.submit();\">\n");
      out.write("                 <input type=button name=prefs value=\"Preferences...\"\n");
      out.write("                        onClick=\"openPrefs();\">\n");
      out.write("                 <input type=submit name=keypad value=\"Keypad\">\n");
      out.write("              ");
 } 
      out.write("\n");
      out.write("              </td>\n");
      out.write("              <input type=hidden name=colorscheme>\n");
      out.write("              <input type=hidden name=font>\n");
      out.write("              <input type=hidden name=render>\n");
      out.write("\t\t      ");
      out.print( sessionState.getTerminalParam(request) );
      out.write("\n");
      out.write("            </form>\n");
      out.write("            </tr>\n");
      out.write("        </table>\n");
      out.write("      </form>\n");
      out.write("\n");
      out.write("    </td>\n");
      out.write("  </tr>\n");
      out.write("</table>\n");
      out.write("\n");
      out.write("  </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
