package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class keys_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.Vector _jspx_dependants;

  public java.util.List getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\n');

  String id = request.getParameter(org.h3270.web.SessionState.TERMINAL);
  if (id == null) {
    id = (String)request.getAttribute(org.h3270.web.SessionState.TERMINAL);
  }
  String screenName = "screen";
  if (id != null) {
    screenName = "screen-" + id;
  }

      out.write("\n");
      out.write("\n");
      out.write("<style type=\"text/css\">\n");
      out.write(".smallkey { width:40px; height:28px; font-size:8pt;}\n");
      out.write(".largekey { width:64px; height:28px; font-size:7pt; }\n");
      out.write("</style>\n");
      out.write("\n");
      out.write("<script>\n");
      out.write("function handleKey (key, id) {\n");
      out.write("  var scr = document.forms[id];\n");
      out.write("  scr.key.value = key;\n");
      out.write("  scr.submit();\n");
      out.write("}\n");
      out.write("\n");
      out.write("function noSuchKey (key) {\n");
      out.write("  window.alert(\"Sorry, this key is not yet implemented.\");\n");
      out.write("}\n");
      out.write("\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("<table border=\"0\" style=\"border-spacing:0px;\">\n");
      out.write("\n");
      out.write("    <tr>\n");
      out.write("    <td style=\"padding:0px;\">\n");
      out.write("  <table>\n");
      out.write("    <!-- function keys -->\n");
      out.write("    <!-- pf1 - pf3 -->\n");
      out.write("    <tr>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf1\">PF1</button></td>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf2\">PF2</button></td>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf3\">PF3</button></td>\n");
      out.write("    </tr>\n");
      out.write("\n");
      out.write("    <!-- pf4 - pf6 -->\n");
      out.write("    <tr>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf4\">PF4</button></td>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf5\">PF5</button></td>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf6\">PF6</button></td>\n");
      out.write("    </tr>\n");
      out.write("\n");
      out.write("    <!-- pf7 - pf9 -->\n");
      out.write("    <tr>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf7\">PF7</button></td>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf8\">PF8</button></td>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf9\">PF9</button></td>\n");
      out.write("    </tr>\n");
      out.write("\n");
      out.write("    <!-- pf10 - pf12 -->\n");
      out.write("    <tr>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf10\">PF10</button></td>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf11\">PF11</button></td>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pf12\">PF12</button></td>\n");
      out.write("    </tr>\n");
      out.write("\n");
      out.write("    <!-- cursor/pa block -->\n");
      out.write("    <tr>\n");
      out.write("      <td></td>\n");
      out.write("      <td><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"smallkey\" name=\"up\">U</button></td>\n");
      out.write("      <td></td>\n");
      out.write("    </tr>\n");
      out.write("    <tr>\n");
      out.write("      <td><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"smallkey\" name=\"left\">L</button></td>\n");
      out.write("      <td><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"smallkey\" name=\"home\">Home</button></td>\n");
      out.write("      <td><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"smallkey\" name=\"right\">R</button></td>\n");
      out.write("    </tr>\n");
      out.write("    <tr>\n");
      out.write("      <td><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"smallkey\" name=\"xxx\">x</button></td>\n");
      out.write("      <td><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"smallkey\" name=\"down\">D</button></td>\n");
      out.write("      <td><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"smallkey\" name=\"xxy\">x</button></td>\n");
      out.write("    </tr>\n");
      out.write("    <tr >\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pa1\">PA1</button></td>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pa2\">PA2</button></td>\n");
      out.write("      <td><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"smallkey\" name=\"pa3\">PA3</button></td>\n");
      out.write("    </tr>\n");
      out.write("\n");
      out.write("  </table>\n");
      out.write("</td><tr>\n");
      out.write("\n");
      out.write("<tr><td style=\"padding:0px;\">\n");
      out.write("  <!-- large keys -->\n");
      out.write("  <table width=100%>\n");
      out.write("    <tr>\n");
      out.write("      <td style=\"padding-top:0px\" align=\"left\" ><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"largekey\" name=\"pos1\">Pos1</button></td>\n");
      out.write("      <td style=\"padding-top:0px\" align=\"right\" ><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"largekey\" name=\"end\">End</button></td>\n");
      out.write("    </tr>\n");
      out.write("    <tr>\n");
      out.write("      <td align=\"left\"><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"largekey\" name=\"clear\">Clear</button></td>\n");
      out.write("      <td align=\"right\"><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"largekey\" name=\"reset\">Reset</button></td>\n");
      out.write("    </tr>\n");
      out.write("    <tr>\n");
      out.write("      <td align=\"left\"><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"largekey\" name=\"eraseEOF\">Erase EOF</button></td>\n");
      out.write("      <td align=\"right\"><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"largekey\" name=\"eraseInput\">Erase Input</button></td>\n");
      out.write("    </tr>\n");
      out.write("    <tr>\n");
      out.write("      <td align=\"left\"><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"largekey\" name=\"dup\">Dup</button></td>\n");
      out.write("      <td align=\"right\"><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"largekey\" name=\"fieldMark\">Field Mark</button></td>\n");
      out.write("    </tr>\n");
      out.write("    <tr>\n");
      out.write("      <td align=\"left\"><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"largekey\" name=\"sysReq\">Sys Req</button></td>\n");
      out.write("      <td align=\"right\"><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"largekey\" name=\"cursorSelect\">Cursor Select</button></td>\n");
      out.write("    </tr>\n");
      out.write("    <tr>\n");
      out.write("      <td align=\"left\"><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"largekey\" name=\"attn\">Attn</button></td>\n");
      out.write("      <td align=\"right\"><button onclick=\"noSuchKey(this.name);\" type=\"button\" class=\"largekey\" name=\"compose\">Compose</button></td>\n");
      out.write("    </tr>\n");
      out.write("    <tr>\n");
      out.write("      <td align=\"left\"><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"largekey\" name=\"newline\">Newline</button></td>\n");
      out.write("      <td align=\"right\"><button onclick=\"handleKey(this.name, '");
      out.print( screenName );
      out.write("');\" type=\"button\" class=\"largekey\" name=\"enter\">Enter</button></td>\n");
      out.write("    </tr>\n");
      out.write("  </table>\n");
      out.write("</td><tr>\n");
      out.write("\n");
      out.write("\n");
      out.write("</table>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
